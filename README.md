Openlabs OpenERPConnector
=========================

This is a very old module for Magento 1 that is required to use 
the Odoo Magento Connector (for Magento 1).

The original link/repository has been lost to time, so we're trying 
to maintain it for those few that need it.

